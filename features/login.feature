@ui
Feature: Login
  In order to prove I can write a behat test
  As a user
  I want to test the login-page

  @javascript
  Scenario: I go to the login page
    Given i am on the login page "/login"
    And I logged with email "malyaoui@gmail.com" and password "12345"
    When I submit the form
    Then I should see the login confirmation
  Scenario: I go to the login page
    Given i am on the login page "/login"
    And I logged with email "alyaoui@gmail.com" and password "1234589"
    When I submit the form
    Then I should see the error confirmation