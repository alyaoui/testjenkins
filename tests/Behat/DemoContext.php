<?php

declare(strict_types=1);

namespace App\Tests\Behat;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\MinkExtension\Context\MinkContext;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
final class DemoContext extends MinkContext
{

    /**
     * @When a demo scenario sends a request to :path
     */
    public function aDemoScenarioSendsARequestTo(string $path): void
    {
        $this->visitPath($path);
        $this->getSession()->wait(5000);
    }


    /**
     * @Then /^I click the login button "([^"]*)"$/
     */
    public function iClickTheLoginButton($button)
    {
        $this->pressButton($button);

    }
}
