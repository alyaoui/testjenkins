<?php
namespace App\Tests\Behat;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\MinkExtension\Context\MinkContext;

/**
* Mink register context.
*/
class MinkLoginContext extends MinkContext
{
    /**
     * @Given /^i am on the login page "([^"]*)"$/
     */
    public function iAmOnTheLoginPage($path)
    {
    $this->visit($path);
    }

    /**
     * @Given /^I logged with email "([^"]*)" and password "([^"]*)"$/
     */
    public function iLoggedWithEmailAndPassword($email, $password)
    {
    $this->fillField('email', $email);
    $this->fillField('password', $password);
    }

    /**
    * @When I submit the form
    */
    public function iSubmitTheForm()
    {
    $this->pressButton('Sign in');
    }


    /**
     * @Then /^I should see the login confirmation$/
     */
    public function iShouldSeeTheLoginConfirmation()
    {
        $this->assertPageContainsText('Congratulations, you are now login!');
        $this->getSession()->wait(5000);
    }

    /**
     * @Then /^I should see the error confirmation$/
     */
    public function iShouldSeeTheErrorConfirmation()
    {
        $this->assertPageContainsText('Invalid credentials.');
        $this->getSession()->wait(5000);
    }


}