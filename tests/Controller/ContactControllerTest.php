<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactControllerTest extends WebTestCase
{
    public function testContact(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/contact');
        $this->assertStringContainsString('ContactController', $crawler->filter('h1')->text());
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }
}
