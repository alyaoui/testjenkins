<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class SecurityControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private UserRepository $repository;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = static::getContainer()->get(UserRepository::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }
    public function testAuthBadCredentials(): void
    {
        $this->client->request('get', '/login');

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Sign in', [
            'email' => 'Testing',
            'password' => 'Testing'
        ]);

        self::assertResponseRedirects('/login');
        $this->client->followRedirect();
        self::assertSelectorExists('.alert.alert-danger');
    }

   /* public function testAuthSuccessfully(): void
    {

        $csrfToken = $this->client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $this->client->request('get', '/login');
        $this->client->submitForm('Sign in', [
            'email' => 'malyaoui@gmail.com',
            'password' => '40544428',
            '_csrf_token'=>$csrfToken
        ]);

        self::assertResponseStatusCodeSame(200);
        self::assertResponseRedirects('/auth');
    }*/
}
