<?php

namespace App\Tests\Entity;


use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\ValidatorBuilder;

class ProductTest extends KernelTestCase
{
    public function getEntity(): Product
    {
        return (new Product())
            ->setPrice(2000)
             ->setType('IT')
             ->setName('PC DELL');
    }
    public function assertHasErrors(Product $product, int $number = 0)
    {
        self::bootKernel();
        $validator  = Validation::createValidatorBuilder()->enableAnnotationMapping()
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();
        $errors = $validator->validate($product);
        $this->assertCount($number, $errors);
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity());
    }

    public function testInvalidCodeEntity()
    {
        $this->assertHasErrors($this->getEntity()->setType(''), 1);
    }
}
